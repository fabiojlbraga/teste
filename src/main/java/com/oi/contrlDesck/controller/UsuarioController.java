package com.oi.contrlDesck.controller;

	
	import java.util.Collection;
	import java.util.HashMap;
	import java.util.Map;
	import java.util.Optional;

	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.http.HttpStatus;
	import org.springframework.http.MediaType;
	import org.springframework.http.ResponseEntity;
	import org.springframework.web.bind.annotation.PathVariable;
	import org.springframework.web.bind.annotation.RequestBody;
	import org.springframework.web.bind.annotation.RequestMapping;
	import org.springframework.web.bind.annotation.RequestMethod;
	import org.springframework.web.bind.annotation.RestController;

import com.oi.contrlDesck.model.Usuario;
import com.oi.contrlDesck.service.UsuarioService;


	@RestController
	public class UsuarioController {
		
		@Autowired
		UsuarioService usuarioService;

		//End points
		@RequestMapping(method = RequestMethod.POST, value = "/clientes", consumes = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Usuario> cadastrarClientes(@RequestBody Usuario usuario) {

			Usuario usuarioCadastrado = usuarioService.cadastrar(usuario);

			return new ResponseEntity<Usuario>(usuarioCadastrado, HttpStatus.CREATED);
		}

		@RequestMapping(method = RequestMethod.GET, value = "/clientes", produces = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Collection<Usuario>> BuscarTodosClientes() {

			Collection<Usuario> usuariosBuscados = usuarioService.buscarTodos();
			

			return new ResponseEntity<>(usuariosBuscados, HttpStatus.OK);
		}

		@RequestMapping(method = RequestMethod.PUT, value = "/clientes", consumes = MediaType.APPLICATION_JSON_VALUE)
		public ResponseEntity<Usuario> alterarUsuario(@RequestBody Usuario usuario) {

			 
			Usuario usuarioAlterado = usuarioService.alterar(usuario);
			return new ResponseEntity<>(usuarioAlterado, HttpStatus.OK);
		}
		
	}

	
	


