package com.oi.contrlDesck;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiWeb02Application {

	public static void main(String[] args) {
		SpringApplication.run(ApiWeb02Application.class, args);
	}
}
