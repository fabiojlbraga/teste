package com.oi.contrlDesck.repository;


import org.springframework.data.jpa.repository.JpaRepository;

import com.oi.contrlDesck.model.Usuario;



public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {

}

