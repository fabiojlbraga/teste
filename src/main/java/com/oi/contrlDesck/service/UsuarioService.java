package com.oi.contrlDesck.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.oi.contrlDesck.model.Usuario;
import com.oi.contrlDesck.repository.UsuarioRepository;


@Service
public class UsuarioService {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	public Usuario cadastrar(Usuario usuario) {

		return usuarioRepository.save(usuario);

	}

	public Collection<Usuario> buscarTodos() {
		return usuarioRepository.findAll();
	}
	
	public void excluir(Usuario usuarioEncontrado){
		usuarioRepository.delete(usuarioEncontrado);
	}
	/*
public Optional<Usuario> buscarPorId(Integer id) {
	return usuarioRepository.findOne(id);
}
*/

	public Usuario alterar(Usuario usuario){
		return usuarioRepository.save(usuario);
	}

}
	
	


