package com.oi.contrlDesck.model;



import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name="perfil")
public class Perfil {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id_perfil;
	private String nome_perfil;
	
	@OneToOne
	private Usuario usuario;

	public int getId_perfil() {
		return id_perfil;
	}

	public void setId_perfil(int id_perfil) {
		this.id_perfil = id_perfil;
	}

	public String getNome_perfil() {
		return nome_perfil;
	}

	public void setNome_perfil(String nome_perfil) {
		this.nome_perfil = nome_perfil;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuarioModel(Usuario usuario) {
		this.usuario = usuario;
	}
	
	
	
}
